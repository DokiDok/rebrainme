Напишите команду для установки пакета jq, в команде необходимо указать ключ подтверждения установки.
В процессе установки не должно появлятся запроса установить программу Y/N ?.
sudo apt install -y jq

Используя apt напишите команду для поиска всех пакетов, имена которых начинаются на postgresql.
Все пакеты которые доступны для установки, а не только установленные пакеты.
apt-cache pkgnames postgresql

Используя apt, найдите и напишите имя пакета, который содержит последнюю версию PostgreSQL сервера в вашей системе, включая версии, доступные в подключённых по умолчанию репозиториях. Имя пакета должно быть вида postgresql-N, где N - одна из основных версий PostgreSQL - 9, 10, 11, 12 ...
apt-cache madison postgresql
postgresql-12

Напишите команды, требуемые для добавления официального репозитория APT для PostgreSQL согласно Wiki (с обновлением списка пакетов), от имени непривилегированного пользователя
# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
# Update the package lists:
sudo apt-get update

Кроме основных версий PostgreSQL, одну из которых вы нашли в 3-м задании, существуют также дополнительные (минорные) версии, например для postgresql-12 есть версии 12.2, 12.4 и т.д. В этом задании требуется посмотреть какие версии существуют для пакета, найденного в 3-м задании. Пришлите команду и вывод команды для нахождения возможных версий пакета, найденного в 3-м задании (предварительно обновив список пакетов для получения пакетов из репозитория PostgreSQL).
$ apt-cache madison postgresql-12
postgresql-12 | 12.8-1.pgdg20.04+1 | http://apt.postgresql.org/pub/repos/apt focal-pgdg/main amd64 Packages
postgresql-12 | 12.8-0ubuntu0.20.04.1 | http://ru.archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages
postgresql-12 | 12.8-0ubuntu0.20.04.1 | http://security.ubuntu.com/ubuntu focal-security/main amd64 Packages
postgresql-12 |     12.2-4 | http://ru.archive.ubuntu.com/ubuntu focal/main amd64 Packages

Напишите команду для установки пакета самой старой версией PostgreSQL, используя список версий, полученный в 5-м задании.
sudo apt install postgresql-12=12.2-4

Напишите команды для скачивания последней версии пакета с https://github.com/gopasspw/gopass/releases при помощи wget и его дальнейшей установки от имени непривилегированного пользователя с установкой всех необходимых зависимостей в автоматическом режиме (без ручных действий).
Найдите на странице ссылку на файл gopass_last-version_linux_amd64.deb
wget https://github.com/gopasspw/gopass/releases/download/v1.12.8/gopass_1.12.8_linux_amd64.deb
sudo apt install /home/rebrainme/mydir/gopass_1.12.8_linux_amd64.deb

Напишите команды для удаления пакета из пункта 7 при помощи apt и dpkg от непривилегированного пользователя в автоматическом режиме (без ручных действий).
sudo apt remove gopass
sudo dpkg -r gopass

Используя утилиту dpkg найдите название виртуального пакета, содержащего утилиту /bin/ss. К ответу приложите команду и вывод.
На странице man dpkg есть описание необходимого ключа.
$ dpkg -S /bin/ss
iproute2: /bin/ss

Используя утилиту apt-cache найдите и пришлите название пакетов, выполняющих установку виртуального пакета ping, - по одному на строку. К ответу приложите команду и вывод.
На странице man apt-cache есть ключ для отображения информации о пакетах.
$ apt-cache showpkg ping
Package: ping
Versions:

Reverse Depends:
  flent,ping
  inetutils-ping:i386,ping
  inetutils-ping,ping
  inetutils-ping:i386,ping
  rancid,ping
  mrtg-ping-probe,ping
  inetutils-ping,ping
  ifupdown-extra,ping
  heartbeat,ping
  gnome-nettool,ping
Dependencies:
Provides:
Reverse Provides:
inetutils-ping 2:1.9.4-11ubuntu0.1 (= )
inetutils-ping 2:1.9.4-11 (= )
iputils-ping 3:20190709-3 (= )




sudo apt install -y jq

apt-cache pkgnames postgresql

apt-cache madison postgresql
postgresql-12

# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
# Update the package lists:
sudo apt-get update

$ apt-cache madison postgresql-12
postgresql-12 | 12.8-1.pgdg20.04+1 | http://apt.postgresql.org/pub/repos/apt focal-pgdg/main amd64 Packages
postgresql-12 | 12.8-0ubuntu0.20.04.1 | http://ru.archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages
postgresql-12 | 12.8-0ubuntu0.20.04.1 | http://security.ubuntu.com/ubuntu focal-security/main amd64 Packages
postgresql-12 |     12.2-4 | http://ru.archive.ubuntu.com/ubuntu focal/main amd64 Packages

sudo apt install postgresql-12=12.2-4

wget https://github.com/gopasspw/gopass/releases/download/v1.12.8/gopass_1.12.8_linux_amd64.deb
sudo apt install /home/rebrainme/mydir/gopass_1.12.8_linux_amd64.deb

sudo apt remove gopass
sudo dpkg -r gopass

$ dpkg -S /bin/ss
iproute2: /bin/ss

$ apt-cache showpkg ping
Package: ping
Versions:

Reverse Depends:
  flent,ping
  inetutils-ping:i386,ping
  inetutils-ping,ping
  inetutils-ping:i386,ping
  rancid,ping
  mrtg-ping-probe,ping
  inetutils-ping,ping
  ifupdown-extra,ping
  heartbeat,ping
  gnome-nettool,ping
Dependencies:
Provides:
Reverse Provides:
inetutils-ping 2:1.9.4-11ubuntu0.1 (= )
inetutils-ping 2:1.9.4-11 (= )
iputils-ping 3:20190709-3 (= )