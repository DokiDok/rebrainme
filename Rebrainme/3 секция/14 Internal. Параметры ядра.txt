Измените конфигурацию максимального количества открытых файлов в системе до 100000 (полная форма команды через CLI).
sysctl -w fs.file-max=100000

В настройках работы с сетевым стеком запретите рассылку ответа при получении широковещательного ICMP-трафика.
sysctl -w net.ipv4.icmp_echo_ignore_broadcasts=1

Измените настройки конфигурации работы со SWAP, запретив системе обращаться к своп-разделу до тех пор, пока не появится реальная угроза вызова OOM-killer и обЪясните выбранное значение.
sysctl -w vm.swappiness=1 (минимальное использование файла подкачки без его полного отключения)
