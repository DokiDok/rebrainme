Обратите внимание, что для типа "Simple" pid-файл не создается автоматически, поэтому нужно добавить соответствующие инструкции в юнит, а также решить вопросы, связанные с повышением привилегий, необходимых при выполнении данного задания (systemd имеет собственный механизм повышения привилегий выполнения команд).

Напишите Systemd Service Unit, который будет запускать приложение /usr/local/bin/node_exporter со следующими параметрами:

Приложение должно перезапускаться при сбоях.
Должно запускаться после готовности network.target.
Должно использовать тип сервиса simple.
Должно считывать переменные окружения из файла /etc/default/node_exporter.
Должно запускаться от имени пользователя prometheus.
Должно хранить PID файл в /var/run/node_exporter.pid (включите в ответ содержимое и атрибуты pid-файла).
Для операции reload должно использовать команду /bin/kill -HUP $MAINPID.


[Unit]
Description=Node Exporter Service
After=network.target

[Service]
User=prometheus
Type=simple
ExecStart=/usr/local/bin/node_exporter
ExecStartPost=+/bin/bash -c 'pgrep node_exporter > /var/run/node_exporter.pid'
ExecReload=/bin/kill -HUP $MAINPID
Restart=on-failure
PIDFile=/var/run/node_exporter.pid
EnvironmentFile=-/etc/default/node_exporter

[Install]
WantedBy=multi-user.target

rebrainme@VM-Ubuntu:/var/run$ ll node_exporter.pid
-rw-r--r-- 1 root root 6 дек 30 14:27 node_exporter.pid

rebrainme@VM-Ubuntu:/var/run$ cat node_exporter.pid
14922