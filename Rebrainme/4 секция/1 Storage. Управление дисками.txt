Подключите к используемой виртуальной машине два дополнительных диска по 8GB.


Создайте на одном из дополнительных дисков таблицу типа MBR, один основной раздел размером 1GB и два логических раздела: на 2GB и на оставшееся место внутри расширенного, используя утилиту fdisk.
sudo fdisk /dev/sdb

Command (m for help): o

Created a new DOS disklabel with disk identifier 0xe10969fc.

Command (m for help): p
Disk /dev/sdb: 8 GiB, 8589934592 bytes, 16777216 sectors
Disk model: VBOX HARDDISK
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xe10969fc

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-16777215, default 2048):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-16777215, default 16777215): +1G

Created a new partition 1 of type 'Linux' and of size 1 GiB.

Command (m for help): n
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): e
Partition number (2-4, default 2): 2
First sector (2099200-16777215, default 2099200):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2099200-16777215, default 16777215):

Created a new partition 2 of type 'Extended' and of size 7 GiB.

Command (m for help): n
All space for primary partitions is in use.
Adding logical partition 5
First sector (2101248-16777215, default 2101248):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2101248-16777215, default 16777215): +2G

Created a new partition 5 of type 'Linux' and of size 2 GiB.

Command (m for help): n
All space for primary partitions is in use.
Adding logical partition 6
First sector (6297600-16777215, default 6297600):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (6297600-16777215, default 16777215):

Created a new partition 6 of type 'Linux' and of size 5 GiB.

Command (m for help): w

The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.

Создайте на втором дополнительном диске таблицу типа GPT и два раздела, выделяя каждому ровно половину свободного пространства, используя утилиту parted.
sudo parted /dev/sdc

(parted) mktable
New disk label type? GPT
(parted) print
Model: ATA VBOX HARDDISK (scsi)
Disk /dev/sdc: 8590MB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags:

Number  Start  End  Size  File system  Name  Flags

(parted) mkpart
Partition name?  []? partone
File system type?  [ext2]? ext4
Start? 0
End? 4295M
Warning: The resulting partition is not properly aligned for best performance: 34s % 2048s != 0s
Ignore/Cancel? i
(parted) mkpart
Partition name?  []? parttwo
File system type?  [ext2]? ext4
Start? 4295M
End? -0M
Warning: The resulting partition is not properly aligned for best performance: 8388672s % 2048s != 0s
Ignore/Cancel? i
(parted) print
Model: ATA VBOX HARDDISK (scsi)
Disk /dev/sdc: 8590MB
Sector size (logical/physical): 512B/512B
Partition Table: gpt
Disk Flags:

Number  Start   End     Size    File system  Name     Flags
 1      17,4kB  4295MB  4295MB  ext4         partone
 2      4295MB  8590MB  4295MB  ext4         parttwo

Приложите логи работы в терминале к ответу.

