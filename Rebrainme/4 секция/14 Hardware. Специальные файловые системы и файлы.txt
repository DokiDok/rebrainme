При помощи sysfs получите размер вашего диска с файловой системой в гигабайтах.
echo $(( `cat /sys/block/sda/sda5/size`*512/1024/1024/1024 ))G

При помощи только procfs узнайте PID текущей запущенной оболочки.
root@VM-Ubuntu:/proc/self# ll /proc/self/cwd
lrwxrwxrwx 1 root root 0 фев  8 15:35 /proc/self/cwd -> /proc/13634/
13634

При помощи procfs получите переменные окружения init сервиса.
cat /proc/1/environ
HOME=/init=/sbin/initNETWORK_SKIP_ENSLAVED=TERM=linuxBOOT_IMAGE=/boot/vmlinuz-5.13.0-27-enericdrop_caps=PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/binPWD=/rootmnt=/root

Напишите скрипт, который позволит получить такой же вывод, что и вызов команды sysctl -a, при помощи /proc (пришлите скрипт и пример его вызова с выводом первых 10 строк).
#!/bin/bash
find /proc/sys -type f > /tmp/f1
file=/tmp/f1
cdr="/proc/sys/dev/cdrom/info"
pref="dev.cdrom.info"
IFS=$'\n'
for var in $(cat $file)
do
        if [ $var = $cdr ]
        then
                cat /proc/sys/dev/cdrom/info > /tmp/f3
                file2="/tmp/f3"
                IFS=$'\n'
                for var2 in $(cat $file2)
                do
                        echo "$pref = $var2" >> /tmp/f4
                done

        continue
        fi
        echo "$var = `cat $var 2> /dev/null`" >> /tmp/f2
done
cut -c 11- /tmp/f2 > /tmp/f3
sed 'y/\//./' /tmp/f3 -i
cat /tmp/f3 >> /tmp/f4
cat /tmp/f4 | sort
        rm /tmp/f1
        rm /tmp/f2
        rm /tmp/f3
        rm /tmp/f4

root@VM-Ubuntu:~/mydir# ./test.sh | head -10
abi.vsyscall32 = 1
debug.exception-trace = 1
debug.kprobes-optimization = 1
dev.cdrom.autoclose = 1
dev.cdrom.autoeject = 0
dev.cdrom.check_media = 0
dev.cdrom.debug = 0
dev.cdrom.info = Can change speed:      1
dev.cdrom.info = Can close tray:                1
dev.cdrom.info = Can lock tray:         1
